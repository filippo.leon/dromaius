#pragma once

#include <array>
#include <cstdint>
#include <stdexcept>

struct Exception : std::runtime_error {
    Exception(const char *msg) : std::runtime_error(msg) {
    }
};

using Byte = std::uint8_t;
using Word = std::uint16_t;

inline constexpr Word ToWord(Byte lsB, Byte msB) { // Little endian
    return lsB | ((Word)msB << 8);
}

inline constexpr bool IsNegative(Byte b) {
    return b & 0x80;
}

class M6502 {
public:
    const Word StackOffset = 0x100;

    M6502() {
    }

    void Reset() {
        m_registers.PC            = LoadWord(0xFFFC);
        m_registers.SP            = 0x0;
        m_registers.status.Unused = 1;
        m_registers.status.I      = 1;
    }

    void ResetMemory() {
        std::fill(m_memory.begin(), m_memory.end(), 0);
    }

    enum class AddressMode {
        Im,
        Zp,
        ZpX,
        ZpY,
        Abs,
        AbsX,
        AbsY,
        IndX,
        IndY,
    };

    enum class Instruction : Byte {
        // Load into A
        LDA_Im   = 0xA9, // Immediate
        LDA_Zp   = 0xA5, // Zero Page
        LDA_ZpX  = 0xB5, // Zero Page, X
        LDA_Abs  = 0xAD, // Absolute
        LDA_AbsX = 0xBD,
        LDA_AbsY = 0xB9,
        LDA_IndX = 0xA1, // Indirect, X (pre-indexed)
        LDA_IndY = 0xB1, // Indirect, Y (post-indexed)

        // Load into X
        LDX_Im   = 0xA2, // Immediate
        LDX_Zp   = 0xA6, // Zero Page
        LDX_ZpY  = 0xB6, // Zero Page, Y
        LDX_Abs  = 0xAE, // Absolute
        LDX_AbsY = 0xBE,

        // Load into X
        LDY_Im   = 0xA0, // Immediate
        LDY_Zp   = 0xA4, // Zero Page
        LDY_ZpX  = 0xB4, // Zero Page, X
        LDY_Abs  = 0xAC, // Absolute
        LDY_AbsX = 0xBC,

        // Store into A
        STA_Zp   = 0x85, // Zero Page
        STA_ZpX  = 0x95, // Zero Page, X
        STA_Abs  = 0x8D, // Absolute
        STA_AbsX = 0x9D,
        STA_AbsY = 0x99,
        STA_IndX = 0x81, // Indirect, X (pre-indexed)
        STA_IndY = 0x91, // Indirect, Y (post-indexed)

        // Store into X
        STX_Zp  = 0x86, // Zero Page
        STX_ZpY = 0x96, // Zero Page, Y
        STX_Abs = 0x8E, // Absolute

        // Store into Y
        STY_Zp  = 0x84, // Zero Page
        STY_ZpX = 0x94, // Zero Page, X
        STY_Abs = 0x8C, // Absolute

        // Add with carry into A
        ADC_Im   = 0x69, // Immediate
        ADC_Zp   = 0x65, // Zero Page
        ADC_ZpX  = 0x75, // Zero Page, X
        ADC_Abs  = 0x6D, // Absolute
        ADC_AbsX = 0x7D,
        ADC_AbsY = 0x79,
        ADC_IndX = 0x61, // Indirect, X (pre-indexed)
        ADC_IndY = 0x71, // Indirect, Y (post-indexed)

        // Subtract with borrow from A
        SBC_Im   = 0xE9, // Immediate
        SBC_Zp   = 0xE5, // Zero Page
        SBC_ZpX  = 0xF5, // Zero Page, X
        SBC_Abs  = 0xED, // Absolute
        SBC_AbsX = 0xFD,
        SBC_AbsY = 0xF9,
        SBC_IndX = 0xE1, // Indirect, X (pre-indexed)
        SBC_IndY = 0xF1, // Indirect, Y (post-indexed)

        // Increment memory
        INC_Zp   = 0xE6, // Zero Page
        INC_ZpX  = 0xF6, // Zero Page, X
        INC_Abs  = 0xEE, // Absolute
        INC_AbsX = 0xFE,

        // Decrement memory
        DEC_Zp   = 0xC6, // Zero Page
        DEC_ZpX  = 0xD6, // Zero Page, X
        DEC_Abs  = 0xCE, // Absolute
        DEC_AbsX = 0xDE,

        // Increment/Decrement registers
        DEX = 0xCA, // Zero Page
        DEY = 0x88, // Zero Page, X
        INX = 0xE8, // Absolute
        INY = 0xC8,

        // Arithmetic shift left
        ASL      = 0x0A, // Register A
        ASL_Zp   = 0x06, // Zero Page
        ASL_ZpX  = 0x16, // Zero Page, X
        ASL_Abs  = 0x0E, // Absolute
        ASL_AbsX = 0x1E,

        // Logical shift right
        LSR      = 0x4A, // Register A
        LSR_Zp   = 0x46, // Zero Page
        LSR_ZpX  = 0x56, // Zero Page, X
        LSR_Abs  = 0x4E, // Absolute
        LSR_AbsX = 0x5E,

        // Rotate left
        ROL      = 0x2A, // Register A
        ROL_Zp   = 0x26, // Zero Page
        ROL_ZpX  = 0x36, // Zero Page, X
        ROL_Abs  = 0x2E, // Absolute
        ROL_AbsX = 0x3E,

        // Rotate right
        ROR      = 0x6A, // Register A
        ROR_Zp   = 0x66, // Zero Page
        ROR_ZpX  = 0x76, // Zero Page, X
        ROR_Abs  = 0x6E, // Absolute
        ROR_AbsX = 0x7E,

        // AND
        AND_Im   = 0x29, // Immediate
        AND_Zp   = 0x25, // Zero Page
        AND_ZpX  = 0x35, // Zero Page, X
        AND_Abs  = 0x2D, // Absolute
        AND_AbsX = 0x3D,
        AND_AbsY = 0x39,
        AND_IndX = 0x21, // Indirect, X (pre-indexed)
        AND_IndY = 0x31, // Indirect, Y (post-indexed)

        // OR
        ORA_Im   = 0x09, // Immediate
        ORA_Zp   = 0x05, // Zero Page
        ORA_ZpX  = 0x15, // Zero Page, X
        ORA_Abs  = 0x0D, // Absolute
        ORA_AbsX = 0x1D,
        ORA_AbsY = 0x19,
        ORA_IndX = 0x01, // Indirect, X (pre-indexed)
        ORA_IndY = 0x11, // Indirect, Y (post-indexed)

        // XOR
        EOR_Im   = 0x49, // Immediate
        EOR_Zp   = 0x45, // Zero Page
        EOR_ZpX  = 0x55, // Zero Page, X
        EOR_Abs  = 0x4D, // Absolute
        EOR_AbsX = 0x5D,
        EOR_AbsY = 0x59,
        EOR_IndX = 0x41, // Indirect, X (pre-indexed)
        EOR_IndY = 0x51, // Indirect, Y (post-indexed)

        // CMP
        CMP_Im   = 0xC9, // Immediate
        CMP_Zp   = 0xC5, // Zero Page
        CMP_ZpX  = 0xD5, // Zero Page, X
        CMP_Abs  = 0xCD, // Absolute
        CMP_AbsX = 0xDD,
        CMP_AbsY = 0xD9,
        CMP_IndX = 0xC1, // Indirect, X (pre-indexed)
        CMP_IndY = 0xD1, // Indirect, Y (post-indexed)

        // CPX
        CPX_Im  = 0xE0, // Immediate
        CPX_Zp  = 0xE4, // Zero Page
        CPX_Abs = 0xEC, // Absolute

        // CPY
        CPY_Im  = 0xC0, // Immediate
        CPY_Zp  = 0xC4, // Zero Page
        CPY_Abs = 0xCC, // Absolute

        // Branch
        BCC = 0x90, // Branch on carry clear
        BCS = 0xB0, // Branch on carry set
        BNE = 0xD0, // Branch on result not zero
        BEQ = 0xF0, // Branch on result zero
        BPL = 0x10, // Branch on result plus
        BMI = 0x30, // Branch on result minus
        BVC = 0x50, // Branch on overflow clear
        BVS = 0x70, // Branch on overflow set

        // Transfer
        TAX = 0xAA, //
        TXA = 0x8A, //
        TAY = 0xA8, //
        TYA = 0x98, //
        TSX = 0xBA, //
        TXS = 0x9A, //

        // Stack
        PHA = 0x48, // Register A
        PLA = 0x68, //
        PHP = 0x08, // Status
        PLP = 0x28, //

        // Subroutine
        JMP_Abs    = 0x4C, // Jump to address
        JMP_AbsInd = 0x6C, // Jump to address indirect
        JSR        = 0x20, // Jump to subroutine
        RTS        = 0x60, // Return from subroutine
        RTI        = 0x40, // Return from interrupt

        // Status
        CLC = 0x18, // Carry clear
        SEC = 0x38, // Carry set
        CLD = 0xD8, // Decimal clear
        SED = 0xF8, // Decimal set
        CLI = 0x58, // Interrupt disable clear
        SEI = 0x78, // Interrupt disable set
        CLV = 0xB8, // Overflow clear

        BRK = 0x00, // Force break
        NOP = 0xEA, // No-op
    };

    Byte &LoadByteZeroPage(Byte zeroPageAddr) {
        m_cycles += 1;
        return m_memory[zeroPageAddr];
    }

    Byte &LoadByte(Word absoluteAddr) {
        m_cycles += 1;
        return m_memory[absoluteAddr];
    }

    Word LoadWord(Word absoluteAddr) {
        m_cycles += 2;
        return ToWord(m_memory[absoluteAddr], m_memory[absoluteAddr + 1]);
    }

    Word WriteData(Word location, Instruction value) {
        return WriteData(location, static_cast<Byte>(value));
    }

    Word WriteData(Word location, Byte value) {
        m_memory[location] = value;
        m_cycles += 1;
        return location + 1;
    }

    Word WriteData(Word location, Word value) {
        m_memory[location]     = static_cast<Byte>(value);
        m_memory[location + 1] = static_cast<Byte>(value >> 8);
        m_cycles += 2;
        return location + 2;
    }

    Word IncrWord(Word w, Byte inc) {
        Word next = w + inc;
        if ((w & 0xFF00) != (next & 0xFF00)) {
            m_cycles += 1;
        }
        return next;
    }

    Byte &FetchNextByte() {
        m_cycles += 1;
        return m_memory[m_registers.PC++];
    }

    Word FetchNextWord() {
        m_cycles += 2;
        Byte lsB = m_memory[m_registers.PC++];
        Byte msB = m_memory[m_registers.PC++];
        return ToWord(lsB, msB);
    }

    Byte &Fetch(AddressMode mode) {
        switch (mode) {
        case AddressMode::Im:
            return FetchNextByte(); // 1 cycle
        case AddressMode::Zp: {
            Byte value = FetchNextByte();
            return LoadByteZeroPage(value);
        } // 2 cycles
        case AddressMode::ZpX: {
            Byte value = FetchNextByte();
            m_cycles += 1; // TODO ?? missing cycle?
            return LoadByteZeroPage(value + m_registers.X);
        } // 3 cycles
        case AddressMode::ZpY: {
            Byte value = FetchNextByte();
            m_cycles += 1; // TODO ?? missing cycle?
            return LoadByteZeroPage(value + m_registers.Y);
        } // 3 cycles
        case AddressMode::Abs: {
            Word value = FetchNextWord();
            return LoadByte(value);
        } // 3 cycles
        case AddressMode::AbsX: {
            Word value = FetchNextWord();
            return LoadByte(IncrWord(value, m_registers.X));
        } // 3* cycles
        case AddressMode::AbsY: {
            Word value = FetchNextWord();
            return LoadByte(IncrWord(value, m_registers.Y));
        } // 3* cycles
        case AddressMode::IndX: {
            Byte value = FetchNextByte();
            m_cycles += 1; // TODO ?? missing cycle?
            return LoadByte(LoadWord(value + m_registers.X));
        } // 5 cycles
        case AddressMode::IndY: {
            Byte value = FetchNextByte();
            return LoadByte(IncrWord(LoadWord(value), m_registers.Y));
        } // 4* cycles
        }
    }

    void PushStack(Byte value) {
        WriteData(m_registers.SP + StackOffset, value);
        m_registers.SP += 1;
    }

    void PushStack(Word value) {
        WriteData(m_registers.SP + StackOffset, value);
        m_registers.SP += 2;
    }

    Byte PopStackByte() {
        m_registers.SP -= 1;
        return LoadByte(m_registers.SP + StackOffset);
    }

    Word PopStackWord() {
        m_registers.SP -= 2;
        return LoadWord(m_registers.SP + StackOffset);
    }

    void LD(Byte value, Byte &reg) {
        reg                  = value;
        m_registers.status.Z = value == 0;
        m_registers.status.N = IsNegative(value);
    }

    void ST(Byte &location, Byte value) {
        location = value;
    }

    void ADC(Byte value) {
        Byte old      = m_registers.A;
        m_registers.A = value + m_registers.A + m_registers.status.C;
        if (!m_registers.status.C && m_registers.A < old ||
            m_registers.status.C && m_registers.A <= old) {
            m_registers.status.C = true;
        } else {
            m_registers.status.C = false;
        }
        m_registers.status.V = (m_registers.A ^ value) & (old ^ value) & 0x80;
        m_registers.status.Z = m_registers.A == 0;
        m_registers.status.N = IsNegative(m_registers.A);
    }

    void SBC(Byte value) {
        Byte old      = m_registers.A;
        m_registers.A = value - m_registers.A + !m_registers.status.C;
        if (!m_registers.status.C && m_registers.A < old ||
            m_registers.status.C && m_registers.A <= old) {
            m_registers.status.C = true;
        } else {
            m_registers.status.C = false;
        }
        m_registers.status.V = (m_registers.A ^ value) & (old ^ value) & 0x80;
        m_registers.status.Z = m_registers.A == 0;
        m_registers.status.N = IsNegative(m_registers.A);
    }

    void INC(Byte &data) {
        data++;
        m_registers.status.Z = data == 0;
        m_registers.status.N = IsNegative(data);
    }

    void DEC(Byte &data) {
        data--;
        m_registers.status.Z = data == 0;
        m_registers.status.N = IsNegative(data);
    }

    void ASL(Byte &data) {
        m_registers.status.C = (data & 0x80);
        data <<= 1;
        m_registers.status.Z = data == 0;
        m_registers.status.N = IsNegative(data);
    }

    void LSR(Byte &data) {
        m_registers.status.C = (data & 0x01);
        data >>= 1;
        m_registers.status.Z = data == 0;
        m_registers.status.N = IsNegative(data);
    }

    void ROL(Byte &data) {
        m_registers.status.C = (data & 0x80);
        data <<= 1;
        data                 = (data % 0xFFFE) & (m_registers.status.C & 0x01);
        m_registers.status.Z = data == 0;
        m_registers.status.N = IsNegative(data);
    }

    void ROR(Byte &data) {
        m_registers.status.C = (data & 0x01);
        data >>= 1;
        data                 = (data % 0xFFFE) & (m_registers.status.C & 0x01);
        m_registers.status.Z = data == 0;
        m_registers.status.N = IsNegative(data);
    }

    void AND(Byte data) {
        m_registers.A &= data;
        m_registers.status.Z = m_registers.A == 0;
        m_registers.status.N = IsNegative(m_registers.A);
    }

    void ORA(Byte data) {
        m_registers.A |= data;
        m_registers.status.Z = m_registers.A == 0;
        m_registers.status.N = IsNegative(m_registers.A);
    }

    void EOR(Byte data) {
        m_registers.A ^= data;
        m_registers.status.Z = m_registers.A == 0;
        m_registers.status.N = IsNegative(m_registers.A);
    }

    void CMP(Byte data, Byte reg) {
        m_registers.status.C = reg >= data;
        m_registers.status.Z = reg == data;
        m_registers.status.N = reg < data;
    }

    void Branch(Byte data, bool cond) {
        if (cond) {
            m_registers.PC += (Word)data - 127;
            m_cycles += 1;
        }
        // +1 cycle if branch taken
        // +1 if on next page (TODO)
    }

    void Transfer(Byte from, Byte &to) {
        to                   = from;
        m_registers.status.Z = to == 0;
        m_registers.status.N = IsNegative(to);
    }

    void Step() {
        Instruction instruction = static_cast<Instruction>(FetchNextByte());
        switch (instruction) {
        /// LDA
        case Instruction::LDA_Im: {
            LD(Fetch(AddressMode::Im), m_registers.A);
            return;
        } // 2 cycles
        case Instruction::LDA_Zp: {
            LD(Fetch(AddressMode::Zp), m_registers.A);
            return;
        } // 3 cycles
        case Instruction::LDA_ZpX: {
            LD(Fetch(AddressMode::ZpX), m_registers.A);
            return;
        } // 4 cycles
        case Instruction::LDA_Abs: {
            LD(Fetch(AddressMode::Abs), m_registers.A);
            return;
        } // 4 cycles
        case Instruction::LDA_AbsX: {
            LD(Fetch(AddressMode::AbsX), m_registers.A);
            return;
        } // 4* cycles
        case Instruction::LDA_AbsY: {
            LD(Fetch(AddressMode::AbsY), m_registers.A);
            return;
        } // 4* cycles
        case Instruction::LDA_IndX: {
            LD(Fetch(AddressMode::IndX), m_registers.A);
            return;
        } // 6 cycles
        case Instruction::LDA_IndY: {
            LD(Fetch(AddressMode::IndY), m_registers.A);
            return;
        } // 5* cycles

        /// LDX
        case Instruction::LDX_Im: {
            LD(Fetch(AddressMode::Im), m_registers.X);
            return;
        } // 2 cycles
        case Instruction::LDX_Zp: {
            LD(Fetch(AddressMode::Zp), m_registers.X);
            return;
        } // 3 cycles
        case Instruction::LDX_ZpY: {
            LD(Fetch(AddressMode::ZpY), m_registers.X);
            return;
        } // 4 cycles
        case Instruction::LDX_Abs: {
            LD(Fetch(AddressMode::Abs), m_registers.X);
            return;
        } // 4 cycles
        case Instruction::LDX_AbsY: {
            LD(Fetch(AddressMode::AbsY), m_registers.X);
            return;
        } // 4* cycles

        /// LDY
        case Instruction::LDY_Im: {
            LD(Fetch(AddressMode::Im), m_registers.Y);
            return;
        } // 2 cycles
        case Instruction::LDY_Zp: {
            LD(Fetch(AddressMode::Zp), m_registers.Y);
            return;
        } // 3 cycles
        case Instruction::LDY_ZpX: {
            LD(Fetch(AddressMode::ZpY), m_registers.Y);
            return;
        } // 4 cycles
        case Instruction::LDY_Abs: {
            LD(Fetch(AddressMode::Abs), m_registers.Y);
            return;
        } // 4 cycles
        case Instruction::LDY_AbsX: {
            LD(Fetch(AddressMode::AbsY), m_registers.Y);
            return;
        } // 4* cycles

        // STA
        case Instruction::STA_Zp: {
            ST(Fetch(AddressMode::Zp), m_registers.A);
            return;
        } // 3 cycles
        case Instruction::STA_ZpX: {
            ST(Fetch(AddressMode::ZpX), m_registers.A);
            return;
        } // 4 cycles
        case Instruction::STA_Abs: {
            ST(Fetch(AddressMode::Abs), m_registers.A);
            return;
        } // 4 cycles
        case Instruction::STA_AbsX: {
            ST(Fetch(AddressMode::AbsX), m_registers.A);
            return;
        } // 5 cycles
        case Instruction::STA_AbsY: {
            ST(Fetch(AddressMode::AbsY), m_registers.A);
            return;
        } // 5 cycles
        case Instruction::STA_IndX: {
            ST(Fetch(AddressMode::IndX), m_registers.A);
            return;
        } // 6 cycles
        case Instruction::STA_IndY: {
            ST(Fetch(AddressMode::IndY), m_registers.A);
            return;
        } // 6 cycles

        // STX
        case Instruction::STX_Zp: {
            ST(Fetch(AddressMode::Zp), m_registers.X);
            return;
        } // 3 cycles
        case Instruction::STX_ZpY: {
            ST(Fetch(AddressMode::ZpY), m_registers.X);
            return;
        } // 4 cycles
        case Instruction::STX_Abs: {
            ST(Fetch(AddressMode::Abs), m_registers.X);
            return;
        } // 4 cycles

        // STY
        case Instruction::STY_Zp: {
            ST(Fetch(AddressMode::Zp), m_registers.Y);
            return;
        } // 3 cycles
        case Instruction::STY_ZpX: {
            ST(Fetch(AddressMode::ZpX), m_registers.Y);
            return;
        } // 4 cycles
        case Instruction::STY_Abs: {
            ST(Fetch(AddressMode::Abs), m_registers.Y);
            return;
        } // 4 cycles

        /// ADC
        case Instruction::ADC_Im: {
            ADC(Fetch(AddressMode::Im));
            return;
        } // 2 cycles
        case Instruction::ADC_Zp: {
            ADC(Fetch(AddressMode::Zp));
            return;
        } // 3 cycles
        case Instruction::ADC_ZpX: {
            ADC(Fetch(AddressMode::ZpX));
            return;
        } // 4 cycles
        case Instruction::ADC_Abs: {
            ADC(Fetch(AddressMode::Abs));
            return;
        } // 4 cycles
        case Instruction::ADC_AbsX: {
            ADC(Fetch(AddressMode::AbsX));
            return;
        } // 4* cycles
        case Instruction::ADC_AbsY: {
            ADC(Fetch(AddressMode::AbsY));
            return;
        } // 4* cycles
        case Instruction::ADC_IndX: {
            ADC(Fetch(AddressMode::IndX));
            return;
        } // 6 cycles
        case Instruction::ADC_IndY: {
            ADC(Fetch(AddressMode::IndY));
            return;
        } // 5* cycles

        /// SBC
        case Instruction::SBC_Im: {
            SBC(Fetch(AddressMode::Im));
            return;
        } // 2 cycles
        case Instruction::SBC_Zp: {
            SBC(Fetch(AddressMode::Zp));
            return;
        } // 3 cycles
        case Instruction::SBC_ZpX: {
            SBC(Fetch(AddressMode::ZpX));
            return;
        } // 4 cycles
        case Instruction::SBC_Abs: {
            SBC(Fetch(AddressMode::Abs));
            return;
        } // 4 cycles
        case Instruction::SBC_AbsX: {
            SBC(Fetch(AddressMode::AbsX));
            return;
        } // 4* cycles
        case Instruction::SBC_AbsY: {
            SBC(Fetch(AddressMode::AbsY));
            return;
        } // 4* cycles
        case Instruction::SBC_IndX: {
            SBC(Fetch(AddressMode::IndX));
            return;
        } // 6 cycles
        case Instruction::SBC_IndY: {
            SBC(Fetch(AddressMode::IndY));
            return;
        } // 5* cycles

        /// INC
        case Instruction::INC_Zp: {
            INC(Fetch(AddressMode::Zp));
            return;
        } // 5 cycles
        case Instruction::INC_ZpX: {
            INC(Fetch(AddressMode::ZpX));
            return;
        } // 6 cycles
        case Instruction::INC_Abs: {
            INC(Fetch(AddressMode::Abs));
            return;
        } // 6 cycles
        case Instruction::INC_AbsX: {
            INC(Fetch(AddressMode::AbsX));
            return;
        } // 7 cycles

        /// DEC
        case Instruction::DEC_Zp: {
            DEC(Fetch(AddressMode::Zp));
            return;
        } // 5 cycles
        case Instruction::DEC_ZpX: {
            DEC(Fetch(AddressMode::ZpX));
            return;
        } // 6 cycles
        case Instruction::DEC_Abs: {
            DEC(Fetch(AddressMode::Abs));
            return;
        } // 6 cycles
        case Instruction::DEC_AbsX: {
            DEC(Fetch(AddressMode::AbsX));
            return;
        } // 7 cycles

        /// Increment/Decrement registers
        case Instruction::INX: {
            INC(m_registers.X);
            return;
        } // 2 cycles
        case Instruction::INY: {
            INC(m_registers.Y);
            return;
        } // 5 cycles
        case Instruction::DEX: {
            DEC(m_registers.X);
            return;
        } // 2 cycles
        case Instruction::DEY: {
            DEC(m_registers.Y);
            return;
        } // 5 cycles

        /// Shift left
        case Instruction::ASL: {
            ASL(m_registers.A);
            return;
        } // 2 cycles
        case Instruction::ASL_Zp: {
            ASL(Fetch(AddressMode::Zp));
            return;
        } // 3 cycles
        case Instruction::ASL_ZpX: {
            ASL(Fetch(AddressMode::ZpX));
            return;
        } // 4 cycles
        case Instruction::ASL_Abs: {
            ASL(Fetch(AddressMode::Abs));
            return;
        } // 4 cycles
        case Instruction::ASL_AbsX: {
            ASL(Fetch(AddressMode::AbsX));
            return;
        } // 4* cycles

        /// Shift right
        case Instruction::LSR: {
            LSR(m_registers.A);
            return;
        } // 2 cycles
        case Instruction::LSR_Zp: {
            LSR(Fetch(AddressMode::Zp));
            return;
        } // 3 cycles
        case Instruction::LSR_ZpX: {
            LSR(Fetch(AddressMode::ZpX));
            return;
        } // 4 cycles
        case Instruction::LSR_Abs: {
            LSR(Fetch(AddressMode::Abs));
            return;
        } // 4 cycles
        case Instruction::LSR_AbsX: {
            LSR(Fetch(AddressMode::AbsX));
            return;
        } // 4* cycles

        /// Rotate left
        case Instruction::ROL: {
            ROL(m_registers.A);
            return;
        } // 2 cycles
        case Instruction::ROL_Zp: {
            ROL(Fetch(AddressMode::Zp));
            return;
        } // 3 cycles
        case Instruction::ROL_ZpX: {
            ROL(Fetch(AddressMode::ZpX));
            return;
        } // 4 cycles
        case Instruction::ROL_Abs: {
            ROL(Fetch(AddressMode::Abs));
            return;
        } // 4 cycles
        case Instruction::ROL_AbsX: {
            ROL(Fetch(AddressMode::AbsX));
            return;
        } // 4* cycles

        /// Rotate right
        case Instruction::ROR: {
            ROR(m_registers.A);
            return;
        } // 2 cycles
        case Instruction::ROR_Zp: {
            ROR(Fetch(AddressMode::Zp));
            return;
        } // 3 cycles
        case Instruction::ROR_ZpX: {
            ROR(Fetch(AddressMode::ZpX));
            return;
        } // 4 cycles
        case Instruction::ROR_Abs: {
            ROR(Fetch(AddressMode::Abs));
            return;
        } // 4 cycles
        case Instruction::ROR_AbsX: {
            ROR(Fetch(AddressMode::AbsX));
            return;
        } // 4* cycles

        /// AND
        case Instruction::AND_Im: {
            AND(Fetch(AddressMode::Im));
            return;
        } // 2 cycles
        case Instruction::AND_Zp: {
            AND(Fetch(AddressMode::Zp));
            return;
        } // 3 cycles
        case Instruction::AND_ZpX: {
            AND(Fetch(AddressMode::ZpX));
            return;
        } // 4 cycles
        case Instruction::AND_Abs: {
            AND(Fetch(AddressMode::Abs));
            return;
        } // 4 cycles
        case Instruction::AND_AbsX: {
            AND(Fetch(AddressMode::AbsX));
            return;
        } // 4* cycles
        case Instruction::AND_AbsY: {
            AND(Fetch(AddressMode::AbsY));
            return;
        } // 4* cycles
        case Instruction::AND_IndX: {
            AND(Fetch(AddressMode::IndX));
            return;
        } // 6 cycles
        case Instruction::AND_IndY: {
            AND(Fetch(AddressMode::IndY));
            return;
        } // 5* cycles

        /// OR
        case Instruction::ORA_Im: {
            ORA(Fetch(AddressMode::Im));
            return;
        } // 2 cycles
        case Instruction::ORA_Zp: {
            ORA(Fetch(AddressMode::Zp));
            return;
        } // 3 cycles
        case Instruction::ORA_ZpX: {
            ORA(Fetch(AddressMode::ZpX));
            return;
        } // 4 cycles
        case Instruction::ORA_Abs: {
            ORA(Fetch(AddressMode::Abs));
            return;
        } // 4 cycles
        case Instruction::ORA_AbsX: {
            ORA(Fetch(AddressMode::AbsX));
            return;
        } // 4* cycles
        case Instruction::ORA_AbsY: {
            ORA(Fetch(AddressMode::AbsY));
            return;
        } // 4* cycles
        case Instruction::ORA_IndX: {
            ORA(Fetch(AddressMode::IndX));
            return;
        } // 6 cycles
        case Instruction::ORA_IndY: {
            ORA(Fetch(AddressMode::IndY));
            return;
        } // 5* cycles

        /// XOR
        case Instruction::EOR_Im: {
            EOR(Fetch(AddressMode::Im));
            return;
        } // 2 cycles
        case Instruction::EOR_Zp: {
            EOR(Fetch(AddressMode::Zp));
            return;
        } // 3 cycles
        case Instruction::EOR_ZpX: {
            EOR(Fetch(AddressMode::ZpX));
            return;
        } // 4 cycles
        case Instruction::EOR_Abs: {
            EOR(Fetch(AddressMode::Abs));
            return;
        } // 4 cycles
        case Instruction::EOR_AbsX: {
            EOR(Fetch(AddressMode::AbsX));
            return;
        } // 4* cycles
        case Instruction::EOR_AbsY: {
            EOR(Fetch(AddressMode::AbsY));
            return;
        } // 4* cycles
        case Instruction::EOR_IndX: {
            EOR(Fetch(AddressMode::IndX));
            return;
        } // 6 cycles
        case Instruction::EOR_IndY: {
            EOR(Fetch(AddressMode::IndY));
            return;
        } // 5* cycles

        /// CMP
        case Instruction::CMP_Im: {
            CMP(Fetch(AddressMode::Im), m_registers.A);
            return;
        } // 2 cycles
        case Instruction::CMP_Zp: {
            CMP(Fetch(AddressMode::Zp), m_registers.A);
            return;
        } // 3 cycles
        case Instruction::CMP_ZpX: {
            CMP(Fetch(AddressMode::ZpX), m_registers.A);
            return;
        } // 4 cycles
        case Instruction::CMP_Abs: {
            CMP(Fetch(AddressMode::Abs), m_registers.A);
            return;
        } // 4 cycles
        case Instruction::CMP_AbsX: {
            CMP(Fetch(AddressMode::AbsX), m_registers.A);
            return;
        } // 4* cycles
        case Instruction::CMP_AbsY: {
            CMP(Fetch(AddressMode::AbsY), m_registers.A);
            return;
        } // 4* cycles
        case Instruction::CMP_IndX: {
            CMP(Fetch(AddressMode::IndX), m_registers.A);
            return;
        } // 6 cycles
        case Instruction::CMP_IndY: {
            CMP(Fetch(AddressMode::IndY), m_registers.A);
            return;
        } // 5* cycles

        /// CPX
        case Instruction::CPX_Im: {
            CMP(Fetch(AddressMode::Im), m_registers.X);
            return;
        } // 2 cycles
        case Instruction::CPX_Zp: {
            CMP(Fetch(AddressMode::Zp), m_registers.X);
            return;
        } // 3 cycles
        case Instruction::CPX_Abs: {
            CMP(Fetch(AddressMode::Abs), m_registers.X);
            return;
        } // 4 cycles

        /// CPY
        case Instruction::CPY_Im: {
            CMP(Fetch(AddressMode::Im), m_registers.Y);
            return;
        } // 2 cycles
        case Instruction::CPY_Zp: {
            CMP(Fetch(AddressMode::Zp), m_registers.Y);
            return;
        } // 3 cycles
        case Instruction::CPY_Abs: {
            CMP(Fetch(AddressMode::Abs), m_registers.Y);
            return;
        } // 4 cycles

        /// Branch
        case Instruction::BCC: {
            Branch(FetchNextByte(), !m_registers.status.C);
            return;
        } // 2 cycles
        case Instruction::BCS: {
            Branch(FetchNextByte(), m_registers.status.C);
            return;
        } // 2 cycles
        case Instruction::BNE: {
            Branch(FetchNextByte(), !m_registers.status.Z);
            return;
        } // 2 cycles
        case Instruction::BEQ: {
            Branch(FetchNextByte(), m_registers.status.Z);
            return;
        } // 2 cycles
        case Instruction::BPL: {
            Branch(FetchNextByte(), !m_registers.status.N);
            return;
        } // 2 cycles
        case Instruction::BMI: {
            Branch(FetchNextByte(), m_registers.status.N);
            return;
        } // 2 cycles
        case Instruction::BVC: {
            Branch(FetchNextByte(), !m_registers.status.V);
            return;
        } // 2 cycles
        case Instruction::BVS: {
            Branch(FetchNextByte(), m_registers.status.V);
            return;
        } // 2 cycles

        /// Transfer
        case Instruction::TAX: {
            Transfer(m_registers.A, m_registers.X);
            return;
        } // 2 cycles
        case Instruction::TXA: {
            Transfer(m_registers.X, m_registers.A);
            return;
        } // 2 cycles
        case Instruction::TAY: {
            Transfer(m_registers.A, m_registers.Y);
            return;
        } // 2 cycles
        case Instruction::TYA: {
            Transfer(m_registers.Y, m_registers.A);
            return;
        } // 2 cycles
        case Instruction::TSX: {
            Transfer(m_registers.SP, m_registers.X);
            return;
        } // 2 cycles
        case Instruction::TXS: {
            Transfer(m_registers.X, m_registers.SP);
            return;
        } // 2 cycles

        /// Stack
        case Instruction::PHA: {
            PushStack(m_registers.A);
            return;
        } // 3 cycles
        case Instruction::PLA: {
            m_registers.A        = PopStackByte();
            m_registers.status.Z = m_registers.A == 0;
            m_registers.status.N = IsNegative(m_registers.A);
            return;
        } // 4 cycles
        case Instruction::PHP: {
            PushStack(m_registers.statusByte);
            return;
        } // 3 cycles
        case Instruction::PLP: {
            m_registers.statusByte =
                (PopStackByte() & 0b11101011) |
                (m_registers.statusByte & 0b00010100); // (ignore break flag and
                                                       // interrupt flag)
            return;
        } // 4 cycles

        /// Subroutine
        case Instruction::JMP_Abs: {
            m_registers.PC = FetchNextWord();
            return;
        } // 3 cycles
        case Instruction::JMP_AbsInd: {
            Word addr      = FetchNextWord();
            m_registers.PC = LoadWord(addr);
            return;
        } // 5 cycles
        case Instruction::JSR: {
            Word value = FetchNextWord();
            m_cycles += 1; // TODO ?? missing cycle?
            PushStack(--m_registers.PC);
            m_registers.PC = value;
            return;
        } // 6 cycles
        case Instruction::RTS: {
            Word value = PopStackWord();
            m_cycles += 3; // TODO ?? missing 3 cycles
            m_registers.PC = value + 1;
            return;
        } // 6 cycles
        case Instruction::RTI: {
            m_registers.statusByte =
                (PopStackByte() & 0b11101011) |
                (m_registers.statusByte & 0b00010100); // (ignore break flag and
                                                       // interrupt flag)
            m_cycles += 3; // TODO ?? missing 3 cycles
            m_registers.PC = PopStackWord();
            return;
        } // 6 cycles

        /// Status
        case Instruction::CLC: { // carry clear
            m_registers.status.C = 0;
            return;
        }                        // 2 cycles
        case Instruction::SEC: { // carry set
            m_registers.status.C = 1;
            return;
        }                        // 2 cycles
        case Instruction::CLD: { // decimal clear
            m_registers.status.D = 0;
            return;
        }                        // 2 cycles
        case Instruction::SED: { // decimal set
            m_registers.status.D = 1;
            return;
        }                        // 2 cycles
        case Instruction::CLI: { // interrupt disable clear
            m_registers.status.I = 0;
            return;
        }                        // 2 cycles
        case Instruction::SEI: { // interrupt disable set
            m_registers.status.I = 1;
            return;
        }                        // 2 cycles
        case Instruction::CLV: { // overflow clear
            m_registers.status.V = 0;
            return;
        } // 2 cycles

        // Misc
        case Instruction::BRK: { // interrupt disable set
            PushStack(Word(m_registers.PC + 2));
            PushStack(m_registers.statusByte);
            m_registers.status.I = 1;
            m_registers.status.B = 1;
            return;
        } // 7 cycles
        case Instruction::NOP: {
            return;
        } // 2 cycles
        }

        throw Exception("Illegal instruction");
    }

    struct Registers {
        Word PC; // Program counter
        Byte A;
        Byte X;
        Byte Y;

        struct StatusFlags {
            Byte N : 1; // Negative
            Byte V : 1; // Overflow
            Byte Unused : 1;
            Byte B : 1; // Break
            Byte D : 1; // Decimal
            Byte I : 1; // Interrupt (IRQ disable)
            Byte Z : 1; // Zero
            Byte C : 1; // Carry
        };

        union {
            StatusFlags status;

            Byte statusByte;
        };

        Byte SP; // Stack pointer
    };

    Word GetPC() const {
        return m_registers.PC;
    }

    Byte GetA() const {
        return m_registers.A;
    }

    int GetCycles() const {
        return m_cycles;
    }

private:
    Registers m_registers;

    std::array<Byte, 64 * 1024> m_memory;

    int m_cycles = 0;
};

constexpr Byte operator""_B(unsigned long long n) {
    return static_cast<Byte>(n);
}

constexpr Word operator""_W(unsigned long long n) {
    return static_cast<Word>(n);
}
