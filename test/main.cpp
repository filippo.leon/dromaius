
#define DOCTEST_CONFIG_IMPLEMENT_WITH_MAIN
#include "doctest/doctest.h"

#include "6502.h"

TEST_CASE("main") {
    M6502 m;

    m.WriteData(0xFFFC, 0x0300_W);

    Word next = m.WriteData(0x0300, M6502::Instruction::JSR);
    next      = m.WriteData(next, 0x0200_W);
    next      = m.WriteData(0x0200, M6502::Instruction::LDA_Im);
    next      = m.WriteData(next, 0x0333_B);
    next      = m.WriteData(next, M6502::Instruction::RTS);

    m.Reset();

    int startCycles = m.GetCycles();
    m.Step();
    m.Step();
    m.Step();

    CHECK(startCycles + 6 + 2 + 6 == m.GetCycles());
    CHECK(0x0333_B == m.GetA());
    CHECK(0x0303_W == m.GetPC());

    // CHECK(factorial(1) == 1);
    // CHECK(factorial(2) == 2);
    // CHECK(factorial(3) == 6);
    // CHECK(factorial(10) == 3628800);
}
